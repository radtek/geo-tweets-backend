from django.conf.urls import url
from geo_tweets import views

urlpatterns = [
    url(r'home$', views.home, name='home'),
]
