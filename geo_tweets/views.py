from django.shortcuts import render

from utils import gmaps_utils
from utils import twitter_utils


def home(request):
    """
    Home view - show geo tweets
    :param request: obj, request
    :return: obj, response
    """
    form_status = ''
    lat = lon = ''
    radius = int(request.POST.get('radius', 1))

    if request.method == 'POST':
        try:
            lat = float(request.POST['lat'])
            lon = float(request.POST['lon'])
            tweets = twitter_utils.get_geo_tweets(lat, lon, max_range=radius)

            map_js = gmaps_utils.generate_map_js(lat, lon, tweets)
            form_status = "Mapped OK"
        except ValueError:
            map_js = gmaps_utils.generate_map_js(0, 0, [])
            form_status = 'Invalid latitude and or longitude'
    else:
        # Some default data to load up map on start
        map_js = gmaps_utils.generate_map_js(0, 0, [])

    context = {'page_title': 'GeoTweets Map', 'map_js': map_js, 'lat': lat, 'lon': lon, 'form_status': form_status,
               'radius': radius}

    return render(request, 'home.html', context)
