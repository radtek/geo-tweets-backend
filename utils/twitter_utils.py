"""Wrapper around twitter-python calls"""

import twitter

import settings

twitter_api = twitter.Api(consumer_key=settings.TWITTER_CONSUMER_KEY,
                          consumer_secret=settings.TWITTER_CONSUMER_SECRET,
                          access_token_key=settings.TWITTER_ACCESS_TOKEN_KEY,
                          access_token_secret=settings.TWITTER_ACCESS_TOKEN_SECRET)


def verify_creds():
    """
    Verify twitter credentials
    :return: dict, api creds verification object
    """
    return twitter_api.VerifyCredentials()


def get_geo_tweets(latitude, longitude, max_range=None):
    """
    Getgeo tweets near
    http://python-twitter.readthedocs.io/en/latest/twitter.html
    :param latitude: str or float, lat
    :param longitude: str or float, lon
    :param max_range: int, maximum range default 1km
    :return: list, resulting tweets
    """
    max_radius = min(max_range or 1, 5)  # search range in kilometres, max 5km
    max_num_results = 50  # max results to obtain
    result = twitter_api.GetSearch(geocode="{},{},{}km".format(latitude, longitude, max_radius),
                                   count=max_num_results)

    return result
