from django.utils.html import escape


def generate_map_js(lat, lon, tweets=None):
    """
    Generates gmaps string used by Gmaps.js
    :param lat:
    :param lon:
    :param tweets:
    :return:
    """
    # Setup map with center
    div_name = '#map'
    map_js = ["""
    var map = new GMaps({{
      div: '{div_name}',
      lat: {lat},
      lng: {lon}
    }});
        """.format(lat=lat, lon=lon, div_name=div_name)]

    # Add each tweet as marker
    tweets = tweets or []
    tweet_count = 0
    for tweet in tweets:
        # Get all tweet attributes we want
        tweet = tweet.__dict__

        if not tweet or 'geo' not in tweet or not tweet['geo'] or 'coordinates' not in tweet['geo']:
            # Make sure we have coordinates for the tweet
            continue
        geo_coordinates = tweet['geo']['coordinates']
        tweet_count += 1

        lat = geo_coordinates[0]
        lon = geo_coordinates[1]
        user = tweet['user'].__dict__['screen_name']
        text = escape(tweet['text']).replace("'", '"').replace('\n', '')
        url = tweet['urls'][0].__dict__['url'] if tweet['urls'] else ""
        created_at = tweet['created_at'][:-11]

        # Add tweet marker
        tweet_js = """
        map.addMarker({{
          lat: {lat},
          lng: {lon},
          title: 'Lima',
          infoWindow: {{
            content: '<h3>{user} ({created_at})</h3><a href={url} target="_blank">{text}</a>'
          }}
        }});
        """.format(lat=lat, lon=lon, user=user, text=text, url=url, created_at=created_at)
        map_js.append(tweet_js)

    if tweet_count:
        # Auto zoom the map based on markers if there were tweets
        map_js.append("map.fitZoom();")

    return ''.join(map_js)
