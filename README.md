# geo_tweets backend

geo tweets backend in django (python 3), with basic UI


# Usage Instructions

* Setup Twitter credentials - https://python-twitter.readthedocs.io/en/latest/getting_started.html
* Download and extract, `cd` to the directory in your terminal
* Update 'local_settings.py' with twitter credentials, see below
* Run: `pip3 install -r requirements.txt`
* Run: `python3 manage.py runserver`
* Navigate to `http://127.0.0.1:8000` in browser


## local_settings.py

Create local_settings.py in the projects base dir where `settings.py` live.
```
# Twitter credentials fill in
twitter_consumer_key = ''
twitter_consumer_secret = ''
twitter_access_token_key = ''
twitter_access_token_secret = ''

```


# Tests

* In root app dir run: `cd tests && python3 ../manage.py test`


# Js Libraries Used

*  Gmaps.js - https://hpneo.github.io/gmaps/examples/markers.html


# Python Environment

* Use python3 (pip3 should also be installed) - https://stackoverflow.com/questions/6587507/how-to-install-pip-with-python-3
* load requirements from included `requirements.txt`


# Things left undone

* No other js libraries/frameworks for UI, maps js generated in backend
* No compression, minification of static files (js, css, images)
* UI features left basic, i.e. tweets don't update as map is scrolled
* No google maps API key gives warning in console
* Whole page refreshes instead of just the map (no ajax call)
* No HTTP server, WSGI config included for production use
* Tests added but left very basic
* Sometimes getting current geo location hangs, even if a timeout is provided
