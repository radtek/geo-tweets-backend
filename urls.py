from django.conf.urls import url
from django.conf.urls import include
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import RedirectView

import geo_tweets.urls
import geo_tweets.views
import geo_tweets

urlpatterns = [
    url(r'^$', geo_tweets.views.home, name='default'),
    url(r'^ui/', include(geo_tweets.urls)),
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/favicon.ico')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
