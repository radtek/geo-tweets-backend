import os

SECRET_KEY = 'rg13hdq!v*awc_)+%)z1y2mv&aa8b4&42u)b2*eagkoba)^+$n'
DEBUG = True

INSTALLED_APPS = (
    'django.contrib.staticfiles',
    'django.contrib.contenttypes',
    'django.contrib.auth',
    'geo_tweets',
)


ROOT_URLCONF = 'urls'

# Grab twitter credentials
try:
    import local_settings
    TWITTER_CONSUMER_KEY = local_settings.twitter_consumer_key
    TWITTER_CONSUMER_SECRET = local_settings.twitter_consumer_secret
    TWITTER_ACCESS_TOKEN_KEY = local_settings.twitter_access_token_key
    TWITTER_ACCESS_TOKEN_SECRET = local_settings.twitter_access_token_secret
except ImportError:
    TWITTER_CONSUMER_KEY = ''
    TWITTER_CONSUMER_SECRET = ''
    TWITTER_ACCESS_TOKEN_KEY = ''
    TWITTER_ACCESS_TOKEN_SECRET = ''
    raise Exception("Please update local_settings.py to include valid twitter_consumer_key, twitter_consumer_secret, "
                    "twitter_access_token_key, twitter_access_token_secret")


BASE_DIR = os.path.dirname(__file__)

STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "geo_tweets", "static"),
]
STATIC_ROOT = os.path.join(BASE_DIR, "static")


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
