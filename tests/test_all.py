#!/usr/bin/env python
"""Tests all methods"""
import logging
import unittest
import os
import sys

from django.test import Client

# Set the path for our application
sys.path.append(os.path.join(os.path.dirname(__file__), '../'))

from utils import gmaps_utils


class TestUtils(unittest.TestCase):
    """Tests utils module"""

    def setUp(self):
        """Sets up before each test"""
        logging.debug('setting up')

    def tearDown(self):
        """Tears down after each test"""
        logging.debug('tearing down')

    def shortDescription(self):
        return None

    # Tests start here

    def test_generate_map_js1(self):
        assert gmaps_utils.generate_map_js(lat=1, lon=2, tweets=[])


class TestClient(unittest.TestCase):
    """Tests views"""

    def setUp(self):
        """Sets up before each test"""
        logging.debug('setting up')

    def tearDown(self):
        """Tears down after each test"""
        logging.debug('tearing down')

    def shortDescription(self):
        return None

    # Tests start here

    def test_client1(self):
        c = Client()
        response = c.post('/', {'lat': '1', 'lon': '2', 'radius': '1'})
        assert response.status_code == 200


if __name__ == '__main__':
    unittest.main()  # pragma: no cover
